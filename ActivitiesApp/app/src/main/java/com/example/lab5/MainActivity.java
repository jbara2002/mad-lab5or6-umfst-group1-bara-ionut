package com.example.lab5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        IntentFilter webFilter = new IntentFilter(Intent.ACTION_VIEW);


        BroadcastReceiver webReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String url = "https://github.com/";
                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(webIntent);
            }
        };

        registerReceiver(webReceiver, webFilter);

        Button webButton = findViewById(R.id.btnGo);
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://github.com/";
                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(webIntent);
            }
        });

        IntentFilter dialFilter = new IntentFilter(Intent.ACTION_DIAL);

        BroadcastReceiver dialReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String phoneNumber = "+40757651371";
                Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null));
                startActivity(dialIntent);
            }
        };

        registerReceiver(dialReceiver, dialFilter);

        Button dialButton = findViewById(R.id.btnDial);
        dialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = "+40757651371";
                Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null));
                startActivity(dialIntent);
            }
        });
        IntentFilter smsFilter = new IntentFilter(Intent.ACTION_VIEW);
        smsFilter.addCategory(Intent.CATEGORY_DEFAULT);
        smsFilter.addDataScheme("sms");

        BroadcastReceiver smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Launch the text messaging intent with the phone number
                String phoneNumber = "+40757651371";
                Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null));
                startActivity(smsIntent);
            }
        };

        registerReceiver(smsReceiver, smsFilter);

        Button smsButton = findViewById(R.id.btnMesaj);
        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = "+40757651371";
                Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null));
                startActivity(smsIntent);
            }
        });

        Button activity2 = findViewById(R.id.btnActivity2);
        activity2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,MainActivity2.class);
                startActivity(intent);
            }
        });
        Button activity3 = findViewById(R.id.btnActivity3);
        activity3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,MainActivity3.class);
                startActivity(intent);
            }
        });
    }
}