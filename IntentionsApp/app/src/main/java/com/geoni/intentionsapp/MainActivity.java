package com.geoni.intentionsapp;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
// camera
        // Define the intent filter for the "Open camera" intent
        IntentFilter cameraFilter = new IntentFilter(MediaStore.ACTION_IMAGE_CAPTURE);

        // Set up the broadcast receiver to handle the "Open camera" intent
        BroadcastReceiver cameraReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Launch the camera app
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                context.startActivity(cameraIntent);
            }
        };

        // Register the "Open camera" receiver with the system
        registerReceiver(cameraReceiver, cameraFilter);

        Button camera = findViewById(R.id.cameraBtn);
        camera.setOnClickListener(view -> {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivity(cameraIntent);
        });

// web search

        // Define the intent filter for the "Web search" intent
        IntentFilter webSearchFilter = new IntentFilter(Intent.ACTION_WEB_SEARCH);

        // Set up the broadcast receiver to handle the "Web search" intent
        BroadcastReceiver webSearchReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Launch the web search intent with the query string
                String query = intent.getStringExtra(SearchManager.QUERY);
                Intent webSearchIntent = new Intent(Intent.ACTION_WEB_SEARCH);
                webSearchIntent.putExtra(SearchManager.QUERY, query);
                startActivity(webSearchIntent);
            }
        };

        registerReceiver(webSearchReceiver, webSearchFilter);
        Button search = findViewById(R.id.webSearchBtn);
        search.setOnClickListener(view -> {
            // Launch the web search intent with a query string
            String query = "android studio";
            Intent webSearchIntent = new Intent(Intent.ACTION_WEB_SEARCH);
            webSearchIntent.putExtra(SearchManager.QUERY, query);
            startActivity(webSearchIntent);
        });
        // sms
        // Define the intent filter for the "Text messaging" intent
        IntentFilter smsFilter = new IntentFilter(Intent.ACTION_VIEW);
        smsFilter.addCategory(Intent.CATEGORY_DEFAULT);
        smsFilter.addDataScheme("sms");

        // Set up the broadcast receiver to handle the "Text messaging" intent
        BroadcastReceiver smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Launch the text messaging intent with the phone number
                String phoneNumber = "555-1234";
                Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null));
                startActivity(smsIntent);
            }
        };

        // Register the "Text messaging" receiver with the system
        registerReceiver(smsReceiver, smsFilter);

        Button sms = findViewById(R.id.messageBtn);
        sms.setOnClickListener(view -> {
            // Launch the text messaging intent with a phone number
            String phoneNumber = "555-1234";
            Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null));
            startActivity(smsIntent);
        });

    }
}

